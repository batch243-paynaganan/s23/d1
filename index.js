// console.log("Pokemon");

// [Section] Objects
/*
	-an object is a data type that is used to represent real world objects.
	-it is a collection of related data and/or functionalities/method.
	-information stored in objects are represented in a "key-value" pair.
	-key is also mostly referred to as a "property" of an object.
	-different data type maybe stored in an object's property creating data structures.
*/

// Creating objects using obbject initializers/literal notation

/*
	Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}

	-this creates/declares and object and also initializes/assign its properties upon creation
	-a cellphone is an example of a real world object.
	-it has its own properties such as name, color, weight, unit model and lot of other properties.
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}
console.log("Result from creating using literal notation: ")
console.log(cellphone);


// Creating objects using constructor function

/*
	-creates reusable function to create several objects but has the same data structure
	-this is useful for creating multiple instances/copies of an object
	-an instance is a concrete occurence of any object which emphasize distinct or unique identity of it.
	Syntax:
	function objectName(valueA, valueB){
		this.keyA = valueA,
		this.keyB = valueB
	}
*/
	/*This is a constructor function*/
	/*this keyword - allows us to assign a new object's properties by associating them with values received from a constructor function's parameter*/
	function Laptop(name, manufactureDate, ram){
		this.laptopName = name;
		this.laptopManufactureDate = manufactureDate;
		this.laptopRam = ram;
	}

	// instatiation
		// the "new" operator creates an instances of an object
		// objects and instances are often interchange because object literals(let object = {}) and instances (let objectName = new functionName(arguments)) are distinct/uniques objects

	let laptop = new Laptop('Lenovo', 2008, "2GB");
	console.log("Result form creating objects using object constructor: ");
	console.log(laptop);

	let myLaptop = new Laptop("Macbook Air", 2020, "2GB");
	console.log("Result form creating objects using object constructor: ");
	console.log(myLaptop);


	let oldLaptop = new Laptop("Portal R2E CCMC", 1980, "500MB")
	// let oldLaptop = Laptop("Portal R2E CCMC", 1980, "500MB")
	/*
		The example above invoke/calls the laptop function instead of ceating a new object
		it will return "undefined" without the "new" operator because the "laptop" function does not have any return statement
	*/
	console.log("Result form creating objects without the new keyword: ");
	console.log(oldLaptop);

	/*Mini-activity*/
	// You will create a constructor function that will let us instatiate a new object, menu, property: menuName, menuPrice
	function menu(mealName, mealPrice){
		this.menuName = mealName;
		this.menuPrice = mealPrice;
	}

	let myOrder = new menu("Chimken", 199);
	console.log("Your order is : ");
	console.log(myOrder);

	// creating empty objects
	let computer = {};
	let myComputer = new Object();
	// let myComputer = new Object("qwe");
	console.log(computer)
	console.log(myComputer)

	// accessing objects inside an array
	let array = [laptop, myLaptop];
	console.log(array);
	console.log(array[0]);
	console.log(array[0]['laptopName']);
	/*dot notation*/
	console.log(array[0].laptopManufactureDate);

	console.log(laptop);
	console.log(laptop.laptopRam);
	console.log(laptop.laptopName);


// [Section] Initializing/adding/deleting/reassigning object properties.
	/*
		-like any other variable in Javascript, objects have their initialized/added after the object was created or declared
	*/

	let car = {};
	console.log(car);
	// initializing adding object properties using dot notation

	car.name = "Honda Civic";
	console.log(car);

	// initializing/adding object using the bracket notation
	car['manufactureDate'] = 2019;
	console.log(car);

	// deleting object properties
		// deleting using bracket notation
	/*delete car["name"];
	console.log(car);*/
		// deleting property using dot notation
	delete car.manufactureDate;
	console.log(car);

	// reassigning object properties
		// reassign object - dot notation
		car.name = "Dodge Charger R/T";
		console.log(car)
		// reassign iobject - bracket notation
		car["name"] = "Jeepney";
		console.log(car)

// [Section] Object Methods
	// a method is function which is a property of an object.
	// they are also functions and one of the key differences they have is that methods are functions related to a specific object

	let person = {
		name: 'John',
		talk: function(){
			console.log("Hello, my name is " + this.name)
		}
	}
	// console.log(person)
	person.talk()

	// add method to objects
	person.walk = function(){
		console.log(this.name + " walked 25 steps forward.")
	}

	person.walk();

	// methods are useful for creating reusable fucntions that perform tasks related to objects
	let friends = {
		firstName: 'Joe',
		lastName: 'Smith',
		address: {
			city: 'Austin',
			state: 'Texas'
		},
		phoneNumber: [['098123145'],['043-3423-4342']],
		emails: ['joe@mail.com', 'joesmite@email.xyz'],
		introduce: function(){
			console.log("Hello my name is " + this.firstName + " " + this.lastName + ". I live in " + this.address.city + ", " + this.address.country + ". My emails are " + this.emails[0] + " and " + this.emails[1] + ". My numbers are " + this.phoneNumber[0][0] + " and " + this.phoneNumber[1][0]);
		}
	}

	friends.introduce()


	/*create an object constructor*/
		function Pokemon(name, level){
			// properties pokemon
			this.pokemonName = name;
			this.pokemonLevel = level
			this.pokemonHealth = 2 * level;
			this.pokemonAttack = level;

			// methods
			this.tackle = function(targetPokemon){
				console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName);
				console.log(this.targetPokemon + "'s is now reduced to" + this.pokemonHealth)
			}

			this.fainted = function(){
				console.log(this.pokemonName + "fainted!")
			}
		}


		let pikachu = new Pokemon("Pikachu", 12);
		console.log(pikachu)
		let gyarados = new Pokemon("Gyarados", 20);
		console.log(gyarados)

		pikachu.tackle(gyarados);
		gyarados.fainted()




